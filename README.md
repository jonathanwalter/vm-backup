# VM Backup

A small script to create live backups of KVM virtual machines (using `virsh`).

It creates a new snapshot of the disk image, copies the "old" disk image, then merges the changes back to the running image.

Released under the Apache License, Version 2.0.

### Getting started

Clone the repo anywhere:

    git clone https://gitlab.com/jonathanwalter/vm-backup.git

Then edit the file and change the parameters under `USER CONFIGURATION`:

    # Log dir
    logdir="/var/log"
    
    # Backup dir
    backupdir="/BACKUP"
    
### Usage

(Output of `vm-backup.sh -h`)


```
  SYNOPSIS
     vm-backup.sh -n [arg] -d [arg] -t [arg]

  DESCRIPTION
      This is a small script to backup virtual machines using virsh.

  OPTIONS
      -n [VIRSHDOMAIN]                   Name of the Virsh Domain
      -d [VIRSHDISK]                     The disk to backup (e.g. vda)
      -t [TYPE]                          The type of backup; NIGHTLY, WEEKLY
                                         It's really just any arbitrary name that
                                         gets appended to the backup.

  EXAMPLES
     vm-backup.sh -n SomeFancyVM -d vda -t WEEKLY
```
