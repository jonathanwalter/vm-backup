#!/usr/bin/env bash

##  LICENSE
##
##  Copyright 2017 Jonathan Walter
##
##  Licensed under the Apache License, Version 2.0 (the "License");
##  you may not use this file except in compliance with the License.
##  You may obtain a copy of the License at
##
##      http://www.apache.org/licenses/LICENSE-2.0
##
##  Unless required by applicable law or agreed to in writing, software
##  distributed under the License is distributed on an "AS IS" BASIS,
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  See the License for the specific language governing permissions and
##  limitations under the License.
##
#%  SYNOPSIS
#+     ${filename} -n [arg] -d [arg] -t [arg]
#%
#%  DESCRIPTION
#%      This is a small script to backup virtual machines using virsh.
#%
#%  OPTIONS
#%      -n [VIRSHDOMAIN]                   Name of the Virsh Domain
#%      -d [VIRSHDISK]                     The disk to backup (e.g. vda)
#%      -t [TYPE]                          The type of backup; NIGHTLY, WEEKLY
#%                                         It's really just any arbitrary name that
#%                                         gets appended to the backup.
#%
#%  EXAMPLES
#%     ${filename} -n SomeFancyVM -d vda -t WEEKLY
#%



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                                                             #
#                             USER CONFIGURATION                              #
#       Log dir                                                               #
        logdir="/var/log"                                                     #
#                                                                             #
#       Backup dir                                                            #
        backupdir="/BACKUP"                                                   #
#                                                                             #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"
__root="$(cd "$(dirname "${__dir}")" && pwd)" # <-- change this as it depends on your app


# define variables
name=""
disk=""
backuptype=""
script_header="$(head -200 "${BASH_SOURCE[0]}" | grep -n "^## END_OF_HEADER" | cut -f1 -d:)"



function root_or_die() {
    if [ $UID != 0 ]; then
        echo "Must be root to run this..."
        exit 1
    fi
}

function usage() { 
    printf "Usage: "; 
    head -${script_header:-99} ${0} | grep -e "^#+" | sed -e "s/^#+[ ]*//g" -e "s/\${filename}/${__base}.sh/g" ;
}

function usagefull() { 
    head -${script_header:-99} ${0} | grep -e "^#[%+-]" | sed -e "s/^#[%+-]//g" -e "s/\${filename}/${__base}.sh/g" ;
}

function check_arguments() {
    if [ -z "$name" ] || [ -z $"$disk" ] || [ -z $"$backuptype" ] ; then
        echo "Arguments -n, -d and -t are all reqired."
        usagefull
        exit 1
    fi
}

function init_logging() {
    logfile="$logdir/$__base/$__base.$name.$(date +%Y-%m).log"

    # Log file subdir
    if [ ! -d $logdir/$__base ]; then
        mkdir -p $logdir/$__base
    fi

}

function log_write() {
    local timestamp=$(date +%d/%b/%Y:%H:%M:%S)
    if read -t 0; then
        cat
    else
        echo "[$timestamp] $*" >> $logfile
    fi
}

function get_disk_path() {
    disk_path=$(virsh domblklist $name | grep $disk | awk '{ print $2 }') >&2 || true
    if [ ! -f $disk_path ] || [ -z $disk_path ]; then 
        echo "Could not find image file associated with $disk"
        exit 1
    fi
}

function check_backup_dir() {
    if [ ! -d $backupdir/$name ]; then
        log_write "Creating directory $backupdir/$name"
        mkdir -p $backupdir/$name  
    fi
}

function create_snapshot() {
    log_write "Creating image snapshot"
    log_write " -- $(virsh snapshot-create-as \
        --domain $name \
        --name $name-$backuptype \
        --diskspec $disk,file=$(dirname $disk_path)/$name-SNAPSHOT.img \
        --disk-only --atomic)"
}

function dump_xml() {
    log_write "Exporting virsh XML to $backupdir/$name/$name-$backuptype.xml"
    virsh dumpxml $name > $backupdir/$name/$name-$backuptype.xml
}

function copy_disk_image() {
    log_write "Copying image file to $backupdir/$name/$name-$backuptype.img..."
    rsync -a $disk_path $backupdir/$name/$name-$backuptype.img 2>> $logfile
    log_write "Done!"
}

function commit_disk_changes() {
    log_write "Commit changes made to the disk while copying"
    log_write "Output: $(virsh blockcommit $name $disk --active --verbose --pivot)"
}

function delete_snapshot() {
    log_write "Deleting temporary snapshot metadata"
    log_write " -- $(virsh snapshot-delete $name $name-$backuptype --metadata)"
}

function delete_snapshot_image() {
    rm -f $(dirname $disk_path)/$name-SNAPSHOT.img 2>> $logfile
    log_write "Deleted temporary snapshot image"
}


function main() {
    # Start timer
    SECONDS=0
    root_or_die
    check_arguments 
    init_logging

    log_write "========== Starting backup of $name =========="
    get_disk_path
    check_backup_dir
    create_snapshot 
    dump_xml
    copy_disk_image
    commit_disk_changes

    log_write "Cleaning up..."
    delete_snapshot
    delete_snapshot_image
    
    duration=$SECONDS
    log_write "========== Done! Runtime $(($duration / 60))m $(($duration % 60))s." 
}

#set -x


# Exit on failure
set -e
# Make sure variables are in order
set -u
# Check for error in the entire pipeline (if you pipe commands)
set -o pipefail
# "Fix" internal field separators
IFS=$'\n\t'

# read arguments 
while getopts ":n:d:t:h" arguments; do
    case $arguments in
        n) name=$OPTARG
        ;;
        d) disk=$OPTARG
        ;;
        t) backuptype=$OPTARG
        ;;
        h) usagefull
            exit 1
        ;;
        \?) echo "Invalid option : -$OPTARG" >&2
            usage
            exit 1
        ;;
        :) echo "Option -$OPTARG requires an argument." >&2
            exit 1
    esac
done

main;
